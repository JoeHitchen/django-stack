# Django Stack

_This project is now deprecated in favour of Django projects that produce a semi-standard Docker image for use with container orchestration tools._

Django Stack provides a simple production-ready, plug-and-play service stack for Django projects.

* Gunicorn application server.
* MySQL database.
* NGINX reverse-proxy and static/media server.

## Setting up Django Stack
Django Stack aims to be as plug-and-play as possible, taking a "It works on my system" application and making it production-ready.
The setup process is as follows:

1. Ensure your application is configured to take production-specific settings from environment variables.  
  _A sample settings file is provided that contains the relevant excerpts._
1. Copy your application into the `src` subdirectory of the Django Stack directory.  
  _You can safely clone an existing project repository and continue using it for version control - Django Stack's git ignores anything in `src`._
1. Create a `.env` file from the template, ensuring (as a minimum) secrets and passwords are changed from the default values.  
  _This file can also include other deployment parameters (e.g. details of other backing services)._

Once set up the stack and be started, or restarted after changes, with `docker-compose up -d --build --force-recreate`.
When using an alternative command, note that the default configuration does not automatically start the database whenever the app is started, to allow support for external databases.
If this is a concern, see the `automatic_db` override sample.

### Database Migrations
Django Stack does not automatically apply migrations because of the risks associated with structural database changes.
They can be applied with one of two commands:

* `docker-compose exec app python manage.py migrate` if the app is running.
* `docker-compose run --rm app python manage.py migrate` if the app is not running.

## Extensions
Extensions and/or modifications to the basic stack are possible by using a `docker-compose.override.yml` file.
Examples are provided in the `override_samples` directory for the following:

* Using an enternal database service (which does not need to be MySQL).  
  _Remember to only call up `app` and `web` if using an external database._
* Automatically calling up the Django Stack-provided database when the `app` service is started.
* Specifing a specific IP range for communications between Django Stack services, and with the host.  
  _This can be useful if host (or additional docker services) have IP-based access policies._

In future, examples may also be provided for:

* Using a task manager, such as Redis & Celery (internal or external).
* A Django Channels based project.

### SSL Termination
This is explicitly out of scope for Django Stack, since we aim to support both production and pre-production requirements.
SSL termination needs either a public domain to be assigned, or self-signed certificates to be generated, and as such provides an unnecessary complication for this project.

If this is a requirement (and it should be for production systems), please see our sister project [Gatekeeper](https://bitbucket.org/JoeHitchen/gatekeeper/).
This provides a configurable, front-of-house reverse-proxy that handles SSL termination and certificates for other services.
