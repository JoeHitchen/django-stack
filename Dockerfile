FROM python:3.7

RUN useradd django
WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV STATIC_ROOT /usr/src/app/static
ENV MEDIA_ROOT /usr/src/app/media
RUN mkdir $STATIC_ROOT && chown -R django:django $STATIC_ROOT \
  && mkdir $MEDIA_ROOT && chown -R django:django $MEDIA_ROOT

RUN pip install mysqlclient gunicorn

COPY --chown=django src/requirements.txt .
RUN pip install -r requirements.txt

COPY --chown=django root_wsgi.py .
COPY --chown=django src .

USER django
